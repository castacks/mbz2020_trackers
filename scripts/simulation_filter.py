#!/usr/bin/env python

import rospy
import math
import tf
import message_filters
from std_msgs.msg import Int16, Header
from nav_msgs.msg import Odometry
from mbz2020_common.msg import LOS, TargetArray, Target
from geometry_msgs.msg import Point, Pose, Vector3, Twist, Quaternion


class SimulationFilter(object):
    def __init__(self):
        rospy.on_shutdown(self.shutdown)

        self.balloons = [
            [(3, 1, 2), False],
            [(9, 5, 2), False],
            [(5, 3, 2), False],
            [(0, 5, 2), False],
            [(7, 7, 2), False],
            [(3, 7, 2), False],
        ]

        self.tracker_count = rospy.get_param("~tracker_count")
        self.subs_los = []
        for i in range(self.tracker_count):
            self.subs_los.append(
                rospy.Subscriber(
                    "/mbz2020_perception/balloon/LOS_" + str(i),
                    LOS,
                    self.check_if_detected,
                )
            )

        self.sub_odom = rospy.Subscriber(
            "/firefly/odometry_sensor1/odometry", Odometry, self.calc_nearest
        )
        self.sub_attack_status = rospy.Subscriber(
            "/mission/planner_attack/status", Int16, self.update_attack_status
        )

        self.pub_targets = rospy.Publisher(
            "/perception/balloon", TargetArray, queue_size=10
        )

        self.viewing = False
        self.nearest_idx = 0
        self.at_target = False
        self.distance_to_closest = 0

    def run(self):
        rate = rospy.Rate(30)

        while not rospy.is_shutdown():

            if self.at_target:
                self.balloons[self.nearest_idx][1] = True

            target_array = TargetArray()

            current_time = rospy.Time.now()

            target = Target()
            target.header.stamp = current_time
            target.id = self.nearest_idx

            los = LOS()
            los.header.stamp = current_time
            los.status = (
                1
                if self.viewing and not self.balloons[self.nearest_idx][1]
                else 0
            )
            los.x = 0
            los.y = 0
            los.xvel = 0
            los.yvel = 0
            target.los = los

            odom = Odometry()
            odom.header.stamp = current_time
            odom_quat = tf.transformations.quaternion_from_euler(0, 0, 0)
            odom.pose.pose = Pose(
                Point(
                    self.balloons[self.nearest_idx][0][0],
                    self.balloons[self.nearest_idx][0][1],
                    self.balloons[self.nearest_idx][0][2],
                ),
                Quaternion(*odom_quat),
            )
            odom.twist.twist = Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))
            target.odom = odom

            target_array.targets = [target]

            self.pub_targets.publish(target_array)

            rospy.loginfo("To closest: {}".format(self.distance_to_closest))

            rate.sleep()

    def calc_nearest(self, odo):
        drone_x = odo.pose.pose.position.x
        drone_y = odo.pose.pose.position.y
        drone_z = odo.pose.pose.position.z

        min_dis = -1
        for idx, balloon_packet in enumerate(self.balloons):
            balloon = balloon_packet[0]
            dis = math.sqrt(
                math.pow(balloon[0] - drone_x, 2)
                + math.pow(balloon[1] - drone_y, 2)
                + math.pow(balloon[2] - drone_z, 2)
            )
            if dis < min_dis or min_dis == -1:
                min_dis = dis
                self.nearest_idx = idx

        self.distance_to_closest = min_dis

    def check_if_detected(self, los):
        self.viewing = True if los.status == 1 else False

    def update_attack_status(self, msg):
        self.at_target = True if msg.data == 1 else False

    def shutdown(self):
        self.sub_odom.unregister()
        for s in self.subs_los:
            s.unregister()


if __name__ == "__main__":
    rospy.init_node("simulation_filter")
    simfil = SimulationFilter()
    simfil.run()
