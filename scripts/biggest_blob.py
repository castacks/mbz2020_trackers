from __future__ import division
import cv2
import numpy as np
import colorsys


class BiggestBlob:
    def __init__(
        self,
        frame,
        color_rgb,
        color_tolerance=5,
        min_contour_size=100,
        min_size_ratio=0.7,
        max_size_ratio=1.4,
        min_mask_score=0.775,
    ):

        self.H_OFFSET = color_tolerance
        self.MIN_CONTOUR_SIZE = min_contour_size
        self.MIN_SIZE_RATIO = min_size_ratio
        self.MAX_SIZE_RATIO = max_size_ratio
        self.MIN_MASK_SCORE = min_mask_score

        self.frame = frame
        self.set_color(color_rgb)

    def detect(self):
        """Function that starts the detection algorithm

        Returns:
            Bouning Box of the detected object -- The bounding box of the object that has been detected or all zeros if no object has been detected.
        """

        # Convert the frame to HSV and apply the mask with the calculated boundaries
        frame_hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(frame_hsv, self.base_hsv, self.top_hsv)
        available_contours = []

        # Find all contours in mask
        _, contours, _ = cv2.findContours(
            mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        for contour in contours:
            # Don't bother with contours smaller than specified size
            if contour.size < self.MIN_CONTOUR_SIZE:
                continue

            # Fit an ellipse around the contour and calculate the height/width ratio
            contour_ellipse = cv2.fitEllipse(contour)

            size_ratio = contour_ellipse[1][0] / contour_ellipse[1][1]

            # Dont bother with any very wide or very high contours
            if (
                size_ratio < self.MIN_SIZE_RATIO
                or size_ratio > self.MAX_SIZE_RATIO
            ):
                continue

            avg = size_ratio

            # Calculate the convex hull of the contour
            hull = cv2.convexHull(contour)
            # Create a blank (black) image
            cimg = np.zeros_like(mask)

            # Draw the convex hull and calculate the area by summing up white pixels
            cv2.drawContours(cimg, [hull], 0, color=255, thickness=-1)
            ct_area = len(np.where(cimg == 255)[0])

            # Create the bitwise and between the convex hull & the mask
            cimg = cv2.bitwise_and(cimg, mask)
            ct_mask_area = len(np.where(cimg == 255)[0])

            # Calculate the ratio between the area the convex hull covers and the area the convex hull && the mask covers
            score = ct_mask_area / ct_area

            if score < self.MIN_MASK_SCORE:
                continue

            # Calculate the average score, the closer to one, the better
            avg = (avg + score) / 2
            available_contours.append((avg, contour))

        if len(available_contours) > 0:
            # Find the contour with the score closest to one
            # best_contour = min(available_contours, key=lambda x: abs(x[0] - 1))[1]

            # Just take the biggest available contour and return it
            best_contour = max(available_contours, key=lambda c: c[1].size)
            x, y, w, h = cv2.boundingRect(best_contour[1])
            return (True, (x, y, w, h))
        else:
            return (False, (0, 0, 0, 0))

    def adjust_bounding_box(self, frame, bbox):
        """Adjusts the bounding box to ballon

        Arguments:
            frame {ndarray} -- The current frame
            bbox {tuple} -- The current bounding box

        Returns:
            bbox -- The new bounding box
        """

        self.frame = frame

        cropped = self.frame[
            int(bbox[1]) : int(bbox[1]) + int(bbox[3]),
            int(bbox[0]) : int(bbox[0]) + int(bbox[2]),
        ]
        cropped_hsv = cv2.cvtColor(cropped, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(cropped_hsv, self.base_hsv, self.top_hsv)

        cropped_size = len(cropped) * len(cropped[0])
        mask_size = len(np.where(mask == 255)[0])

        ratio = mask_size / cropped_size
        if ratio > 0.7 or ratio < 0.6:
            return self.detect()
        else:
            return bbox

    def set_frame(self, frame):
        """Sets the frame the detection algorithm should run on

        Arguments:
            frame {numpy array} -- The new frame
        """

        self.frame = frame

    def set_color(self, color_rgb):
        """Sets the color the detection algorithm should look for

        Arguments:
            color_rgb {Tuple (int, int, int)} -- A (r, g, b) tuple
        """

        self.color_rgb = np.uint8(
            [[[color_rgb[0], color_rgb[1], color_rgb[2]]]]
        )
        self.color_hsv = cv2.cvtColor(self.color_rgb, cv2.COLOR_RGB2HSV)

        self.base_hsv = [self.color_hsv[0][0][0] - self.H_OFFSET, 100, 100]
        self.base_hsv = [0 if i < 0 else i for i in self.base_hsv]
        self.base_hsv = np.array(self.base_hsv)

        self.top_hsv = [self.color_hsv[0][0][0] + self.H_OFFSET, 255, 255]
        self.top_hsv = [255 if i > 255 else i for i in self.top_hsv]
        self.top_hsv = np.array(self.top_hsv)
