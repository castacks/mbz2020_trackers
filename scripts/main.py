#!/usr/bin/env python
from __future__ import division
import cv2
import numpy as np
from biggest_blob import BiggestBlob
from kcf import KCFTracker
import random
import matplotlib.pyplot as plt
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image as cam_img
import std_msgs.msg
from std_msgs.msg import String, Int16
from dynamic_reconfigure.server import Server, decode_config
from mbz2020_common.msg import LOS
import os
import time


class balloon_tracker:
    def __init__(self):
        self.init_ros_params()
        self.init_publishers()
        self.init_subscriptions()

        self.color_to_track = (self.color_r, self.color_g, self.color_b)

        self.detected = False
        self.scale_factor = 1
        self.already_lower = False
        self.need_restart = False
        self.recently_restarted = False
        self.counter_sc_restart = 0
        self.track_ok = False
        self.ready = False
        self.color = self.random_color()

        self.max_bbox_size = 20000
        self.min_fps = 30
        self.fallback_scale = 0.7
        self.default_scale = 0.7
        self.bounding_box_resize = 1
        self.manual_restart_delay = 15
        self.color_tolerance = 5
        self.min_contour_size = 100
        self.min_size_ratio = 0.85
        self.max_size_ratio = 1.15
        self.min_mask_score = 0.8

    def init_ros_params(self):
        self.sub_topic = rospy.get_param(
            "/mbz2020_perception/balloon/image_topic"
        )
        self.id = rospy.get_param("~tracker_id")

        self.color_r = rospy.get_param("~color_r")
        self.color_g = rospy.get_param("~color_g")
        self.color_b = rospy.get_param("~color_b")

    def init_subscriptions(self):
        self.image_sub = rospy.Subscriber(
            self.sub_topic, cam_img, self.got_frame
        )
        self.sub_hunt_status = rospy.Subscriber(
            "/mission/planner_hunt/status", Int16, self.update_hunt_status
        )

    def init_publishers(self):
        prefix = "/mbz2020_perception/balloon/"
        self.los_pub = rospy.Publisher(
            prefix + "LOS_" + str(self.id), LOS, queue_size=10
        )

    def run(self):
        rate = rospy.Rate(30)

        while not rospy.is_shutdown():
            rate.sleep()

    def update_hunt_status(self, message):
        self.ready = True if message.data == 0 else False

    def random_color(self):
        return (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
        )

    def got_frame(self, data):
        if not self.ready:
            return

        bridge = CvBridge()
        frame = bridge.imgmsg_to_cv2(data, desired_encoding="passthrough")
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        frame = cv2.resize(
            frame,
            (
                int(frame.shape[1] * self.scale_factor),
                int(frame.shape[0] * self.scale_factor),
            ),
        )

        t = time.time()

        if not self.detected:
            self.detector = BiggestBlob(
                frame,
                self.color_to_track,
                color_tolerance=self.color_tolerance,
                min_contour_size=self.min_contour_size,
                min_size_ratio=self.min_size_ratio,
                max_size_ratio=self.max_size_ratio,
                min_mask_score=self.min_mask_score,
            )
            self.detected, bbox = self.detector.detect()
            self.tracker = KCFTracker(frame, bbox)

        # fps_timeline = []
        # frame_count = 0

        self.track_ok, bbox = self.tracker.update(frame)

        # If the tracker could't find anything, restart the detector
        frame_count = 0
        if not self.track_ok:
            frame_count = 0
            self.detector.set_frame(frame)
            self.detected, bbox = self.detector.detect()
            self.tracker.restart(frame, bbox)
            self.color = self.random_color()
        else:
            frame_count = frame_count + 1

        # Force a resize of the bounding box every X Seconds to avoid overhead or a too small bbox
        if frame_count > self.bounding_box_resize:
            frame_count = 0
            bbox_new = self.detector.adjust_bounding_box(frame, bbox)
            if bbox_new != bbox:
                bbox = bbox_new
                self.tracker.restart(frame, bbox)

        # Calculate the center of the bbox and draw it
        bbox_center = (int(bbox[0] + bbox[2] / 2), int(bbox[1] + bbox[3] / 2))
        cv2.circle(frame, bbox_center, 3, (0, 255, 0), 3)
        # Calculate LOS
        img_width = int(frame.shape[1])
        img_height = int(frame.shape[0])
        img_center = (int(img_width / 2), int(img_height / 2))
        los_coordinates = (
            bbox_center[0] - img_center[0],
            bbox_center[1] - img_center[1],
        )
        # Mark the center of the image
        cv2.circle(frame, img_center, 3, (0, 0, 255), 3)
        # Draw LOS
        cv2.line(frame, bbox_center, img_center, (0, 0, 255), 2, 1)

        # Draw the bounding box on the current frame and display it
        p1 = (int(bbox[0]), int(bbox[1]))
        p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
        cv2.rectangle(frame, p1, p2, self.color, 2, 1)

        message_header = std_msgs.msg.Header()
        message_header.stamp = rospy.Time.now()

        status = 1 if self.track_ok else 0
        self.los_pub.publish(
            message_header,
            status,
            int(los_coordinates[0]),
            int(los_coordinates[1]),
            0,
            0,
        )

        # Calculate the time it took to process the last frame
        fps = 1 / (time.time() - t)

        # When the bounding box is too big or the fps are too low set the flag to manually restart the tracker with a new detection
        performance_low = (
            float(bbox[2] * bbox[3]) / float(frame.shape[0] * frame.shape[1])
        ) > self.max_bbox_size or fps < self.min_fps
        if performance_low:
            if self.recently_restarted and self.already_lower:
                self.counter_sc_restart = 0
            else:
                self.scale_factor = self.fallback_scale
                if not self.already_lower:
                    self.need_restart = True
                self.already_lower = True
        elif not self.recently_restarted:
            self.scale_factor = self.default_scale
            if self.already_lower:
                self.need_restart = True
            self.already_lower = False

        # To avoid a manual restart every frame, only allow it after a certain amount of frames
        if self.recently_restarted:
            self.counter_sc_restart = self.counter_sc_restart + 1

        if self.counter_sc_restart > self.manual_restart_delay:
            self.counter_sc_restart = 0
            self.recently_restarted = False

        # If the flag for a manual restart has been set, do it
        if self.need_restart:
            frame_count = 0
            self.detector.set_frame(frame)
            self.detected, bbox = self.detector.detect()
            self.tracker.restart(frame, bbox)
            self.color = self.random_color()
            self.need_restart = False
            self.recently_restarted = True


if __name__ == "__main__":
    rospy.init_node("tracker")
    b_tracker = balloon_tracker()
    b_tracker.run()
