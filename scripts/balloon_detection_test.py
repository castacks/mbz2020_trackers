#!/usr/bin/env python

import rospy
import std_msgs.msg
from mbz2020_common.msg import TargetArray, Target, LOS
from nav_msgs.msg import Odometry


class fake_balloon(object):
    def __init__(self):
        rospy.on_shutdown(self.shutdown)
        self.quick_user_interrupt = False

        # variables
        self.odom = Odometry()
        self.tolerance = [0.1, 0.1, 0.1]

        # Publishers
        self.pub_balloon = rospy.Publisher(
            "/perception/balloon", TargetArray, queue_size=10
        )

        # Subscribers
        self.sub_odom = rospy.Subscriber(
            "/firefly/odometry_sensor1/odometry", Odometry, self.odom_callback
        )

    def run(self):
        rate = rospy.Rate(30)

        start = rospy.get_time()

        waypoint = [3.0, 2.0, 1.0]

        popped_flag = 0

        while not rospy.is_shutdown():

            now = rospy.get_time() - start
            # rospy.loginfo('time = {}'.format(now))

            if self.has_reached(waypoint):
                popped_flag = 1

            # only publish fake balloon for a few seconds
            if now > 5.0 and now < 10.0 and popped_flag == 0:

                # create and publish fake balloon
                header = std_msgs.msg.Header()
                header.stamp = rospy.Time.now()
                balloon = Target()
                balloon.header = header
                balloon.id = 0
                balloon.odom = Odometry()
                balloon.odom.pose.pose.position.x = waypoint[0]
                balloon.odom.pose.pose.position.y = waypoint[1]
                balloon.odom.pose.pose.position.z = waypoint[2]
                balloon.los = LOS()
                balloon.los.status = 1
                msg_balloon = TargetArray()
                msg_balloon.targets = [balloon]
                self.pub_balloon.publish(msg_balloon)

            else:

                header = std_msgs.msg.Header()
                header.stamp = rospy.Time.now()
                balloon = Target()
                balloon.header = header
                balloon.id = 0
                balloon.los = LOS()
                balloon.los.status = 0
                msg_balloon = TargetArray()
                msg_balloon.targets = [balloon]
                self.pub_balloon.publish(msg_balloon)

            rate.sleep()

    def has_reached(self, waypoint):
        x_done = (
            abs(self.odom.pose.pose.position.x - waypoint[0])
            < self.tolerance[0]
        )
        y_done = (
            abs(self.odom.pose.pose.position.y - waypoint[1])
            < self.tolerance[1]
        )
        z_done = (
            abs(self.odom.pose.pose.position.z - waypoint[2])
            < self.tolerance[2]
        )
        return x_done and y_done and z_done

    def odom_callback(self, msg):
        self.odom = msg

    def shutdown(self):
        # unregister subscribers
        self.sub_odom.unregister()

        # kill
        self.quick_user_interrupt = True


if __name__ == "__main__":
    try:
        rospy.init_node("fake_balloon", anonymous=True)
        FAKE_BALLOON = fake_balloon()
        FAKE_BALLOON.run()
    except rospy.ROSInterruptException:
        pass
