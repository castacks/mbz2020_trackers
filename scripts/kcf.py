import cv2


class KCFTracker:
    def __init__(self, frame, bbox):
        self.tracker = cv2.TrackerKCF_create()
        self.tracker.init(frame, bbox)

    def update(self, frame):
        bbox = self.tracker.update(frame)
        return bbox

    def restart(self, frame, bbox):
        self.tracker = cv2.TrackerKCF_create()
        self.tracker.init(frame, bbox)
