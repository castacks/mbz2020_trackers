#!/usr/bin/env python

import rospy
import message_filters
from sensor_msgs.msg import Image
from mbz2020_common.msg import LOS
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time


class Viewer:
    def __init__(self, pub):
        self.los = None
        self.publisher = pub

    def callback(self, image, los_0, los_1):
        bridge = CvBridge()
        frame = bridge.imgmsg_to_cv2(image, "rgb8")
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        if los_0.status is 1:
            img_width = int(frame.shape[1])
            img_height = int(frame.shape[0])
            img_center = (int(img_width / 2), int(img_height / 2))
            img_coords = (
                int(los_0.x + img_center[0]),
                int(los_0.y + img_center[1]),
            )
            cv2.circle(frame, img_coords, 10, (255, 255, 0), 10, 1)

        if los_1.status is 1:
            img_width = int(frame.shape[1])
            img_height = int(frame.shape[0])
            img_center = (int(img_width / 2), int(img_height / 2))
            img_coords = (
                int(los_1.x + img_center[0]),
                int(los_1.y + img_center[1]),
            )
            cv2.circle(frame, img_coords, 10, (0, 255, 0), 10, 1)

        if los_0.status is 0 and los_1.status is 0:
            cv2.putText(
                frame,
                "Tracking failure",
                (10, 50),
                cv2.FONT_HERSHEY_DUPLEX,
                1,
                (0, 0, 255),
            )

        self.publisher.publish(bridge.cv2_to_imgmsg(frame, "bgr8"))

    def im_callback(self, image):
        bridge = CvBridge()
        frame = bridge.imgmsg_to_cv2(image, desired_encoding="passthrough")
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        if self.los is not None:
            img_width = int(frame.shape[1])
            img_height = int(frame.shape[0])
            img_center = (int(img_width / 2), int(img_height / 2))
            img_coords = (
                int(self.los.x + img_center[0]),
                int(self.los.y + img_center[1]),
            )
            cv2.circle(frame, img_coords, 3, (0, 0, 255), 2, 1)
        else:
            print("LOS None")

        cv2.imshow("test", frame)
        cv2.waitKey(1)

    def los_callback(self, los):
        self.los = los


if __name__ == "__main__":
    rospy.init_node("track_viewer")

    prefix = "/mbz2020_perception/balloon/"
    publisher = rospy.Publisher(prefix + "viewer", Image, queue_size=1)

    v = Viewer(publisher)

    sub_topic = rospy.get_param("/mbz2020_perception/balloon/image_topic")
    image_sub = message_filters.Subscriber(sub_topic, Image)
    los_sub_0 = message_filters.Subscriber(prefix + "LOS_0", LOS)
    los_sub_1 = message_filters.Subscriber(prefix + "LOS_1", LOS)

    ts = message_filters.ApproximateTimeSynchronizer(
        [image_sub, los_sub_0, los_sub_1], 1, 1
    )
    ts.registerCallback(v.callback)

    rospy.spin()
